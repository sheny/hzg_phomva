#include <fstream>
#include <TH1D.h>
#include <TRandom.h>
#include <TLorentzVector.h>
#include <TCanvas.h>
#include <TLegend.h>
#include "select_pho.h"
#include <TStyle.h>
#include <algorithm>
#include "untuplizer_07.h"
#include"sele_pho.h"
void xana(const char* input,const char* output){
  TreeReader data(input);
  gStyle->SetOptStat(1111111);
  TFile f(output,"recreate");
  TTree mEvent("mEvent","photon");
  Float_t mc_Pt,weight,wei,w,sceta;
  Float_t DeltaR,delR,dR;
  Float_t reco_Pt,reco_Eta,reco_Phi,Sceta,r9,rawE,sigmaIEtaIEta,Phiwidth,Etawidth,sigmaIEtaIPhi,s4,sigmaRR,Rho,phoIso,chIso,chworstIso,esEnP1,esEnP2;
  TLorentzVector mc,reco;
  TLorentzVector reco_pho,mc_pho;
  mEvent.Branch("mc_Pt",&mc_Pt,"mc_Pt/F");
  mEvent.Branch("reco_Pt",&reco_Pt,"reco_Pt/F");
  mEvent.Branch("DeltaR",&DeltaR,"DeltaR/F");
  mEvent.Branch("reco_Eta",&reco_Eta,"reco_Eta/F");
  mEvent.Branch("reco_Phi",&reco_Phi,"reco_Phi/F");
  mEvent.Branch("Sceta",&Sceta,"Sceta/F");
  mEvent.Branch("weight",&weight,"weight/F");
  mEvent.Branch("wei",&wei,"wei/F");
  mEvent.Branch("w",&w,"w/F");
  mEvent.Branch("r9",&r9,"r9/F");
  mEvent.Branch("rawE",&rawE,"rawE/F");
  mEvent.Branch("sigmaIEtaIEta",&sigmaIEtaIEta,"sigmaIEtaIEta/F");
  mEvent.Branch("Phiwidth",&Phiwidth,"Phiwidth/F");
  mEvent.Branch("Etawidth",&Etawidth,"Etawidth/F");
  mEvent.Branch("sigmaIEtaIPhi",&sigmaIEtaIPhi,"sigmaIEtaIPhi/F");
  mEvent.Branch("s4",&s4,"s4/F");
  mEvent.Branch("sigmaRR",&sigmaRR,"sigmaRR/F");
  mEvent.Branch("Rho",&Rho,"Rho/F");
  mEvent.Branch("phoIso",&phoIso,"phoIso/F");
  mEvent.Branch("chIso",&chIso,"chIso/F");
  mEvent.Branch("chworstIso",&chworstIso,"chworstIso/F");
  mEvent.Branch("esEnP1",&esEnP1,"esEnP1/F");
  mEvent.Branch("esEnP2",&esEnP2,"esEnP2/F");

  for ( Long64_t ev = 0; ev < data.GetEntriesFast(); ev++) {
    if (ev % 100000 == 0)
      fprintf(stderr, "Processing event %lli of %lli\n", ev + 1, data.GetEntriesFast());

    data.GetEntry(ev);
    Float_t* mcPt    = data.GetPtrFloat("mcPt");
    Float_t* phoEt   = data.GetPtrFloat("phoEt");
    Float_t* mcEta    = data.GetPtrFloat("mcEta");
    Float_t* phoEta   = data.GetPtrFloat("phoEta");
    Float_t* mcPhi    = data.GetPtrFloat("mcPhi");
    Float_t* phoPhi   = data.GetPtrFloat("phoPhi");
    Float_t* mcE     = data.GetPtrFloat("mcE");
    Float_t* phoE     = data.GetPtrFloat("phoE");
    Int_t   nMC  = data.GetInt("nMC");
    Int_t  nPho  = data.GetInt("nPho");
    Int_t* mcMomPID = data.GetPtrInt("mcMomPID");
    Float_t* phoSCEta = data.GetPtrFloat("phoSCEta");
    Float_t* phoSCRawE = data.GetPtrFloat("phoSCRawE");
    Float_t* phoR9 = data.GetPtrFloat("phoR9");
    Float_t* phoSigmaIEtaIEtaFull5x5 = data.GetPtrFloat("phoSigmaIEtaIEtaFull5x5");
    Float_t* phoSCPhiWidth = data.GetPtrFloat("phoSCPhiWidth");
    Float_t* phoSCEtaWidth = data.GetPtrFloat("phoSCEtaWidth");
    Float_t* phoSigmaIEtaIPhiFull5x5 = data.GetPtrFloat("phoSigmaIEtaIPhiFull5x5");
    Float_t* phoE2x2Full5x5 = data.GetPtrFloat("phoE2x2Full5x5");
    Float_t* phoESEffSigmaRR = data.GetPtrFloat("phoESEffSigmaRR");
    Float_t rho = data.GetFloat("rho");
    Float_t* phoPFPhoIso = data.GetPtrFloat("phoPFPhoIso");
    Float_t* phoPFChIso = data.GetPtrFloat("phoPFChIso");
    Float_t* phoPFChWorstIso = data.GetPtrFloat("phoPFChWorstIso");
    Float_t* phoESEnP1 = data.GetPtrFloat("phoESEnP1");
    Float_t* phoESEnP2 = data.GetPtrFloat("phoESEnP2");
    vector<int> acc_pho;
    vector<int> acc_pho_reco;
    vector<int> deltaR;

    select_pho(data, acc_pho);
    sele_pho(data, acc_pho_reco);

    if(acc_pho.size()<1) continue;
    mc.SetPtEtaPhiE(mcPt[acc_pho[0]],mcEta[acc_pho[0]],mcPhi[acc_pho[0]],mcE[acc_pho[0]]);
    mc_Pt = mc.Pt();

    deltaR.clear();
    for(Int_t j = 0;j<acc_pho_reco.size();j++){
      reco.SetPtEtaPhiE(phoEt[acc_pho_reco[j]],phoEta[acc_pho_reco[j]],phoPhi[acc_pho_reco[j]],phoE[acc_pho_reco[j]]);
      dR = mc.DeltaR(reco);
      if(dR>0.1)continue;
      deltaR.push_back(j);
      reco_pho = reco;
      delR = dR;
      sceta = phoSCEta[acc_pho_reco[j]];
      r9 = phoR9[acc_pho_reco[j]];
      rawE = phoSCRawE[acc_pho_reco[j]];
      sigmaIEtaIEta = phoSigmaIEtaIEtaFull5x5[acc_pho_reco[j]];
      Phiwidth = phoSCPhiWidth[acc_pho_reco[j]];
      Etawidth = phoSCEtaWidth[acc_pho_reco[j]];
      sigmaIEtaIPhi = phoSigmaIEtaIPhiFull5x5[acc_pho_reco[j]];
      s4 = phoE2x2Full5x5[acc_pho_reco[j]];
      sigmaRR = phoESEffSigmaRR[acc_pho_reco[j]];
      phoIso = phoPFPhoIso[acc_pho_reco[j]];
      chIso = phoPFChIso[acc_pho_reco[j]];
      chworstIso = phoPFChWorstIso[acc_pho_reco[j]];
      esEnP1 = phoESEnP1[acc_pho_reco[j]];
      esEnP2 = phoESEnP2[acc_pho_reco[j]];
    }

    // cout<<"==="<<mcMomPID[acc_pho_reco[0]]<<"==="<<mcEta[acc_pho[0]]<<"==="<<mcPhi[acc_pho[0]]<<"==="<<mcPt[acc_pho[0]]<<endl;
    if(deltaR.size()<1)continue;
    reco_Pt = reco_pho.Pt();
    reco_Eta = reco_pho.Eta();
    reco_Phi = reco_pho.Phi();
    Sceta = sceta;
    DeltaR = delR;
    Rho = rho;
    weight = 35.4*0.001*137751/17678702;
    wei = 35.4*0.001*154500/42345735;
    w = 35.4*0.001*16792/79243357;
    mEvent.Fill();

  }
  mEvent.Write();
}
